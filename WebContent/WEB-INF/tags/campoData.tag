<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ attribute name="id" required="true" %>
<%@ attribute name="value" required="true" %>
<input id="${id}" name="${id}" type="text" value="<fmt:formatDate value="${value}" pattern="dd/MM/yyyy" />">

<script>
$("#${id}").datepicker({dateFormat: 'dd/mm/yy'});
</script>