<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Cadastro de Cliente</title>
<style>
.nome.ng-valid {
	background-color: lightgreen;
}

.nome.ng-dirty.ng-invalid-required {
	background-color: red;
}

.nome.ng-dirty.ng-invalid-minlength {
	background-color: yellow;
}

.cpf.ng-valid {
	background-color: lightgreen;
}

.cpf.ng-dirty.ng-invalid-required {
	background-color: red;
}

.cpf.ng-dirty.ng-invalid-minlength {
	background-color: yellow;
}

.email.ng-valid {
	background-color: lightgreen;
}

.email.ng-dirty.ng-invalid-required {
	background-color: red;
}

.email.ng-dirty.ng-invalid-email {
	background-color: yellow;
}

.telefone.ng-valid {
	background-color: lightgreen;
}

.telefone.ng-dirty.ng-invalid-required {
	background-color: red;
}

.telefone.ng-dirty.ng-invalid-minlength {
	background-color: yellow;
}
</style>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="<c:url value='/resources/static/css/app.css' />"
	rel="stylesheet"></link>
</head>
<body ng-app="myApp" class="ng-cloak">
	<div class="generic-container"
		ng-controller="ClienteController as ctrl">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="lead">Formul�rio de registro de Cliente</span>
			</div>
			<div class="formcontainer">
				<form ng-submit="ctrl.submit()" name="myForm"
					class="form-horizontal">
					<input type="hidden" ng-model="ctrl.cliente.id" />
					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 control-lable" for="file">Nome</label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.cliente.nome" name="uname"
									class="nome form-control input-sm"
									placeholder="Entre com nome do cliente" required
									ng-minlength="3" />
								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.uname.$error.required">This is a
										required field</span> <span ng-show="myForm.uname.$error.minlength">Minimum
										length required is 3</span> <span ng-show="myForm.uname.$invalid">This
										field is invalid </span>
								</div>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 control-lable" for="file">cpf</label>
							<div class="col-md-7">
								<input type="text" ng-model="ctrl.cliente.cpf"
									class="form-control input-sm"
									placeholder="Entre com CPF do cliente" />
								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.cpf.$error.required">This is a
										required field</span> <span ng-show="myForm.cpf.$invalid">This
										field is invalid </span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 control-lable" for="file">Email</label>
							<div class="col-md-7">
								<input type="email" ng-model="ctrl.cliente.email" name="email"
									class="email form-control input-sm"
									placeholder="Entre com email do cliente" required />
								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.email.$error.required">This is a
										required field</span> <span ng-show="myForm.email.$invalid">This
										field is invalid </span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 control-lable" for="file">telefone</label>
							<div class="col-md-7">
							<form name="form1">
								<input type="text" maxlength="14"
									ng-model="ctrl.cliente.telefone" class="form-control input-sm"
									placeholder="Digite seu n�mero de telefone (xxx)xxxxxxxx" />
								<div class="has-error" ng-show="myForm.$dirty">
									<span ng-show="myForm.telefone.$error.required">This is
										a required field</span> <span ng-show="myForm.telefone.$invalid">This
										field is invalid </span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-actions floatRight">
							<input type="submit"
								value="{{!ctrl.cliente.id ? 'Add' : 'Update'}}"
								class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
							<button type="button" ng-click="ctrl.reset()"
								class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset
								Form</button>
							<a href="menu.html"><button type="button"
									class="btn btn-success custom-width">Voltar</button></a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">
				<span class="lead">Lista de Clientes </span>
			</div>
			<div class="tablecontainer">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>ID.</th>
							<th>Name</th>
							<th>Cpf</th>
							<th>Email</th>
							<th>Telefone</th>
							<th width="20%"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="c in ctrl.clientes">
							<td><span ng-bind="c.id"></span></td>
							<td><span ng-bind="c.nome"></span></td>
							<td><span ng-bind="c.cpf"></span></td>
							<td><span ng-bind="c.email"></span></td>
							<td><span ng-bind="c.telefone"></span></td>
							<td>
								<button type="button" ng-click="ctrl.edit(c.id)"
									class="btn btn-success custom-width">Edit</button>
								<button type="button" ng-click="ctrl.remove(c.id)"
									class="btn btn-danger custom-width">Remove</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
	<script src="<c:url value='/resources/static/js/app.js' />"></script>
	<script
		src="<c:url value='/resources/static/js/service/cliente_service.js' />"></script>
	<script
		src="<c:url value='/resources/static/js/controller/cliente_controller.js' />"></script>
</body>
</html>