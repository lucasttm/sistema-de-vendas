<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Cadastro de Produto</title>  
    <style>
      .nome.ng-valid {
          background-color: lightgreen;
      }
      .nome.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .nome.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }
       .fabricante.ng-valid {
          background-color: lightgreen;
      }
      .fabricante.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .fabricante.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }


    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link href="<c:url value='/resources/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="ProdutoController as ctrl">
          <div class="panel panel-default">
              <div class="panel-heading"><span class="lead">Formul�rio de registro de Produto</span></div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.produto.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Nome</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.produto.nome" name="nome" class="nome form-control input-sm" placeholder="Entre com nome do produto" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.nome.$error.required">Esse � um campo obrigat�rio</span>
                                      <span ng-show="myForm.nome.$error.minlength">Tamanho m�nimo obrigat�rio � 3</span>
                                      <span ng-show="myForm.nome.$invalid">Esse campo est� inv�lido.</span>
                                  </div>
                              </div>
                          </div>
                      </div>
                        
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Fabricante</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.produto.fabricante" class="form-control input-sm" placeholder="Entre com o nome do Fabricante."/>
                              	  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.fabricante.$error.required">Esse campo � obrigat�rio.</span>
                                      <span ng-show="myForm.fabricante.$invalid">Esse campo est� inv�lido.</span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{!ctrl.produto.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Limpar</button>
                              <a href="menu.html"><button  type="button"  class="btn btn-success custom-width">Voltar</button></a>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista de Produtos</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Nome</th>
                              <th>Fabricante</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in ctrl.produtos">
                              <td><span ng-bind="u.id"></span></td>
                              <td><span ng-bind="u.nome"></span></td>
                              <td><span ng-bind="u.fabricante"></span></td>
                              <td>
                              <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success custom-width">Editar</button>  <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Remove</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
      <script src="<c:url value='/resources/static/js/app.js' />"></script>
      <script src="<c:url value='/resources/static/js/service/produto_service.js' />"></script>
      <script src="<c:url value='/resources/static/js/controller/produto_controller.js' />"></script>
  </body>
</html>