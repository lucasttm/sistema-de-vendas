<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Cadastro de Venda</title>  
    <style>
      .cliente.ng-valid {
          background-color: lightgreen;
      }
      .cliente.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .cliente.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }
       .produto.ng-valid {
          background-color: lightgreen;
      }
      .produto.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .produto.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }
       .quantidade.ng-valid {
          background-color: lightgreen;
      }
      .quantidade.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .quantidade.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }

    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link href="<c:url value='/resources/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="VendaController as ctrl">
          <div class="panel panel-default">
              <div class="panel-heading"><span class="lead">Formulário de registro de Vendas</span></div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.venda.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Cliente</label>
                              <div class="col-md-7" ng-controller="ClienteController as clienteCtrl">
                              	  <select id="ClientSelector" style="width: 100%;" class="name form-control input-sm" 
                              	  ng-options="option.nome for option in clienteCtrl.clientes track by option.id"
                              	  ng-model="ctrl.venda.cliente">
   							 	  </select>
                              
                              </div>
                          </div>
                      </div>
                        
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Produto</label>
                              <div class="col-md-7" ng-controller="ProdutoController as produtoCtrl">
                                  <select id="ProdutoSelector" style="width: 100%;" class="name form-control input-sm" 
                                  ng-options="option.nome for option in produtoCtrl.produtos track by option.id"
                                  ng-model="ctrl.venda.produto">
      							    
   							 	  </select>
                              </div>
                          </div>
                      </div>
                     <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Quantidade</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.venda.quantidade" class="form-control input-sm" placeholder="Informe a quantidade"/>
                              	  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.quantidade.$error.required">This is a required field</span>
                                      <span ng-show="myForm.quantidade.$invalid">This field is invalid </span>
                                  </div>	 
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{!ctrl.venda.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                              <a href="menu.html"><button  type="button"  class="btn btn-success custom-width">Voltar</button></a>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista de Vendas</span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID.</th>
                              <th>Cliente</th>
                              <th>Produto</th>
                              <th>Quantidade</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="v in ctrl.vendas">
                              <td><span ng-bind="v.id"></span></td>
                              <td><span ng-bind="v.cliente.nome"></span></td>
                              <td><span ng-bind="v.produto.nome"></span></td>
                              <td><span ng-bind="v.quantidade"></span></td>
                              <td>
                              <button type="button" ng-click="ctrl.edit(v.id)" class="btn btn-success custom-width">Edit</button>  <button type="button" ng-click="ctrl.remove(v.id)" class="btn btn-danger custom-width">Remove</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
      <script src="<c:url value='/resources/static/js/app.js' />"></script>
      <script src="<c:url value='/resources/static/js/service/venda_service.js' />"></script>
      <script src="<c:url value='/resources/static/js/controller/venda_controller.js' />"></script>
      <script src="<c:url value='/resources/static/js/controller/cliente_controller.js' />"></script>
      <script src="<c:url value='/resources/static/js/service/cliente_service.js' />"></script>
      <script src="<c:url value='/resources/static/js/controller/produto_controller.js' />"></script>
      <script src="<c:url value='/resources/static/js/service/produto_service.js' />"></script>
  </body>
</html>