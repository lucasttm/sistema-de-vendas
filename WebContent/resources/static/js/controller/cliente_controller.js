'use strict';

App.controller('ClienteController', ['$scope', 'ClienteService', function($scope, ClienteService) {
          var self = this;
          self.cliente={id:null,nome:'',cpf:'',email:'',telefone:''};
          self.clientes=[];
              
          self.fetchAllClientes = function(){
              ClienteService.fetchAllClientes()
                  .then(
      					       function(d) {
      						        self.clientes = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
           
          self.createCliente = function(cliente){
              ClienteService.createCliente(cliente)
		              .then(
                      self.fetchAllClientes, 
				              function(errResponse){
					               console.error('Error while creating Cliente.');
				              }	
                  );
          };

         self.updateCliente = function(cliente, id){
              ClienteService.updateCliente(cliente, id)
		              .then(
				              self.fetchAllClientes, 
				              function(errResponse){
					               console.error('Error while updating Cliente.');
				              }	
                  );
          };

         self.deleteCliente = function(id){
              ClienteService.deleteCliente(id)
		              .then(
				              self.fetchAllClientes, 
				              function(errResponse){
					               console.error('Error while deleting Cliente.');
				              }	
                  );
          };

          self.fetchAllClientes();

          self.submit = function() {
              if(self.cliente.id==null){
                  console.log('Saving New Cliente', self.cliente);    
                  self.createCliente(self.cliente);
              }else{
                  self.updateCliente(self.cliente, self.cliente.id);
                  console.log('Cliente updated with id ', self.cliente.id);
              }
              self.reset();
          };
              
          self.edit = function(id){
              console.log('id to be edited', id);
              for(var i = 0; i < self.clientes.length; i++){
                  if(self.clientes[i].id == id) {
                     self.cliente = angular.copy(self.clientes[i]);
                     break;
                  }
              }
          };
              
          self.remove = function(id){
              console.log('id to be deleted', id);
              if(self.cliente.id === id) {
            	  //clean form if the cliente to be deleted is shown there.
                 self.reset();
              }
              self.deleteCliente(id);
          };

          
          self.reset = function(){
              self.cliente={id:null,nome:'',cpf:'',email:'',telefone:''};
              $scope.myForm.$setPristine(); //reset Form
          };

      }]);
