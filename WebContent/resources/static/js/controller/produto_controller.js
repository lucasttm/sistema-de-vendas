'use strict';

App.controller('ProdutoController', ['$scope', 'ProdutoService', function($scope, ProdutoService) {
          var self = this;
          self.produto={id:null,nome:'',fabricante:''};
          self.produtos=[];
              
          self.fetchAllProdutos = function(){
              ProdutoService.fetchAllProdutos()
                  .then(
      					       function(d) {
      						        self.produtos = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
           
          self.createProduto = function(produto){
              ProdutoService.createProduto(produto)
		              .then(
                      self.fetchAllProdutos, 
				              function(errResponse){
					               console.error('Error while creating Produto.');
				              }	
                  );
          };

         self.updateProduto = function(produto, id){
              ProdutoService.updateProduto(produto, id)
		              .then(
				              self.fetchAllProdutos, 
				              function(errResponse){
					               console.error('Error while updating Produto.');
				              }	
                  );
          };

         self.deleteProduto = function(id){
              ProdutoService.deleteProduto(id)
		              .then(
				              self.fetchAllProdutos, 
				              function(errResponse){
					               console.error('Error while deleting Produto.');
				              }	
                  );
          };

          self.fetchAllProdutos();

          self.submit = function() {
              if(self.produto.id==null){
                  console.log('Saving New Produto', self.produto);    
                  self.createProduto(self.produto);
              }else{
                  self.updateProduto(self.produto, self.produto.id);
                  console.log('Produto updated with id ', self.produto.id);
              }
              self.reset();
          };
              
          self.edit = function(id){
              console.log('id to be edited', id);
              for(var i = 0; i < self.produtos.length; i++){
                  if(self.produtos[i].id == id) {
                     self.produto = angular.copy(self.produtos[i]);
                     break;
                  }
              }
          };
              
          self.remove = function(id){
              console.log('id to be deleted', id);
              if(self.produto.id === id) {//clean form if the produto to be deleted is shown there.
                 self.reset();
              }
              self.deleteProduto(id);
          };

          
          self.reset = function(){
              self.produto={id:null,nome:'',fabricante:''};
              $scope.myForm.$setPristine(); //reset Form
          };

      }]);
