'use strict';

App.controller('VendaController', ['$scope', 'VendaService', function($scope, VendaService) {
          var self = this;
          self.venda={id:null,cliente:null,produto:null,quantidade:''};
          self.vendas=[];
              
          self.fetchAllVendas = function(){
              VendaService.fetchAllVendas()
                  .then(
      					       function(d) {
      						        self.vendas = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
           
          self.createVenda = function(venda){
              VendaService.createVenda(venda)
		              .then(
                      self.fetchAllVendas, 
				              function(errResponse){
					               console.error('Error while creating Venda.');
				              }	
                  );
          };

         self.updateVenda = function(venda, id){
              VendaService.updateVenda(venda, id)
		              .then(
				              self.fetchAllVendas, 
				              function(errResponse){
					               console.error('Error while updating Venda.');
				              }	
                  );
          };

         self.deleteVenda = function(id){
              VendaService.deleteVenda(id)
		              .then(
				              self.fetchAllVendas, 
				              function(errResponse){
					               console.error('Error while deleting Venda.');
				              }	
                  );
          };

          self.fetchAllVendas();

          self.submit = function() {
              if(self.venda.id==null){
                  console.log('Saving New Venda', self.venda);    
                  self.createVenda(self.venda);
              }else{
                  self.updateVenda(self.venda, self.venda.id);
                  console.log('Venda updated with id ', self.venda.id);
              }
              self.reset();
          };
              
          self.edit = function(id){
              console.log('id to be edited', id);
              for(var i = 0; i < self.vendas.length; i++){
                  if(self.vendas[i].id == id) {
                     self.venda = angular.copy(self.vendas[i]);
                     break;
                  }
              }
          };
              
          self.remove = function(id){
              console.log('id to be deleted', id);
              if(self.venda.id === id) {//clean form if the venda to be deleted is shown there.
                 self.reset();
              }
              self.deleteVenda(id);
          };

          
          self.reset = function(){
              self.venda={id:null,cliente:null,produto:null,quantidade:''};
              $scope.myForm.$setPristine(); //reset Form
          };

      }]);
