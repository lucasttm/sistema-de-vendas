'use strict';

App.factory('ClienteService', ['$http', '$q', function($http, $q){
	return {
		
			fetchAllClientes: function() {
					return $http.get('http://localhost:8080/vendas/cliente/')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching clientes');
										return $q.reject(errResponse);
									}
							);
			},
		    
		    createCliente: function(cliente){
					return $http.post('http://localhost:8080/vendas/cliente/', cliente)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating cliente');
										return $q.reject(errResponse);
									}
							);
		    },
		    
		    updateCliente: function(cliente, id){
					return $http.put('http://localhost:8080/vendas/cliente/'+id, cliente)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while updating cliente');
										return $q.reject(errResponse);
									}
							);
			},
		    
			deleteCliente: function(id){
					return $http.delete('http://localhost:8080/vendas/cliente/'+id)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while deleting cliente');
										return $q.reject(errResponse);
									}
							);
			}
		
	};

}]);