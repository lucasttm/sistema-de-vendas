'use strict';

App.factory('ProdutoService', ['$http', '$q', function($http, $q){
	return {
		
			fetchAllProdutos: function() {
					return $http.get('http://localhost:8080/vendas/produto/')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching produtos');
										return $q.reject(errResponse);
									}
							);
			},
		    
		    createProduto: function(produto){
					return $http.post('http://localhost:8080/vendas/produto/', produto)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating produto');
										return $q.reject(errResponse);
									}
							);
		    },
		    
		    updateProduto: function(produto, id){
					return $http.put('http://localhost:8080/vendas/produto/'+id, produto)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while updating produto');
										return $q.reject(errResponse);
									}
							);
			},
		    
			deleteProduto: function(id){
					return $http.delete('http://localhost:8080/vendas/produto/'+id)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while deleting produto');
										return $q.reject(errResponse);
									}
							);
			}
		
	};

}]);