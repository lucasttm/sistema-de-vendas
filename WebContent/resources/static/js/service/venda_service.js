'use strict';

App.factory('VendaService', ['$http', '$q', function($http, $q){
	return {
		
			fetchAllVendas: function() {
					return $http.get('http://localhost:8080/vendas/venda/')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Erro ao buscar uma venda');
										return $q.reject(errResponse);
									}
							);
			},
		    
		    createVenda: function(venda){
					return $http.post('http://localhost:8080/vendas/venda/', venda)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Erro ao criar uma venda');
										return $q.reject(errResponse);
									}
							);
		    },
		    
		    updateVenda: function(venda, id){
					return $http.put('http://localhost:8080/vendas/venda/'+id, venda)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Erro enquanto atualizava a venda');
										return $q.reject(errResponse);
									}
							);
			},
		    
			deleteVenda: function(id){
					return $http.delete('http://localhost:8080/vendas/venda/'+id)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Erro enquanto deletava a venda');
										return $q.reject(errResponse);
									}
							);
			}
		
	};

}]);