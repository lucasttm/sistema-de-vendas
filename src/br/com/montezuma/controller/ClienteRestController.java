package br.com.montezuma.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.montezuma.dao.ClienteDao;
import br.com.montezuma.modelo.Cliente;

@RestController
public class ClienteRestController {

    @Autowired
    ClienteDao clienteDao;

    // -------------------Retrieve All
    // Clients--------------------------------------------------------

    @RequestMapping(value = "/cliente/", method = RequestMethod.GET)
    public ResponseEntity<List<Cliente>> listaTodosClientes() {
	List<Cliente> clientes = clienteDao.buscaTodos();
	if (clientes.isEmpty()) {
	    return new ResponseEntity<List<Cliente>>(HttpStatus.NO_CONTENT);
	}
	return new ResponseEntity<List<Cliente>>(clientes, HttpStatus.OK);
    }

    // -------------------Retrieve Single
    // Client--------------------------------------------------------

    @RequestMapping(value = "/client/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Cliente> getCliente(@PathVariable("id") long id) {
	System.out.println("Fetching Client with id " + id);
	Cliente cliente = clienteDao.buscaPeloId(id);
	if (cliente == null) {
	    System.out.println("Client with id " + id + " not found");
	    return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
	}
	return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
    }

    // -------------------Create a
    // Client--------------------------------------------------------

    @RequestMapping(value = "/cliente/", method = RequestMethod.POST)
    public ResponseEntity<Void> createClient(@RequestBody Cliente cliente, UriComponentsBuilder ucBuilder) {
	System.out.println("Creating Client " + cliente.getNome());

	if (clienteDao.buscaPeloNome(cliente.getNome()) != null) {
	    System.out.println("A Client with name " + cliente.getNome() + " already exist");
	    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}

	clienteDao.salva(cliente);

	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(ucBuilder.path("/client/{id}").buildAndExpand(cliente.getId()).toUri());
	return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    // ------------------- Update a Client
    // --------------------------------------------------------

    @RequestMapping(value = "/cliente/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Cliente> atualizaCliente(@PathVariable("id") long id, @RequestBody Cliente cliente) {

	Cliente currentClient = clienteDao.buscaPeloId(id);

	if (currentClient == null) {
	    System.out.println("Cliente with id " + id + " not found");
	    return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
	}
	currentClient.setNome(cliente.getNome());
	currentClient.setCpf(cliente.getCpf());
	currentClient.setEmail(cliente.getEmail());
	currentClient.setTelefone(cliente.getTelefone());

	clienteDao.atualiza(currentClient);
	return new ResponseEntity<Cliente>(currentClient, HttpStatus.OK);
    }

    // ------------------- Delete a Client
    // --------------------------------------------------------

    @RequestMapping(value = "/cliente/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Cliente> deleteClient(@PathVariable("id") long id) {
	System.out.println("Fetching & Deleting Client with id " + id);

	Cliente cliente = clienteDao.buscaPeloId(id);
	if (cliente == null) {
	    System.out.println("Unable to delete. Client with id " + id + " not found");
	    return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
	}

	clienteDao.delete(id);
	return new ResponseEntity<Cliente>(HttpStatus.NO_CONTENT);
    }

}