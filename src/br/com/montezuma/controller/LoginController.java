package br.com.montezuma.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.montezuma.modelo.Usuario;

@Controller
public class LoginController {

    @RequestMapping("loginForm")
    public String login() {
	return "formulario-login";
    }

    @RequestMapping("efetuaLogin")
    public String efetuaLogin(Usuario usuario, HttpSession session) {
	if (usuario != null && "admin".equals(usuario.getUsername()) && "admin".equals(usuario.getPassword())) {
	    session.setAttribute("usuarioLogado", usuario);
	    return "redirect:menu.html";
	} else {
	    return "redirect:loginForm";
	}
    }

    @RequestMapping("logout")
    public String logout(HttpSession session) {
	session.invalidate();
	return "redirect:loginForm";
    }

    @RequestMapping("UserManagement")
    public String userManagement() {
	return "UserManagement";
    }

}
