package br.com.montezuma.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MenuController {
    @RequestMapping("ClienteManagement")
    public String clienteManagement() {
	return "ClienteManagement";
    }

    @RequestMapping("ProdutoManagement")
    public String produtoManagement() {
	return "ProdutoManagement";
    }

    @RequestMapping("VendaManagement")
    public String vendaManagement() {
	return "VendaManagement";
    }

}
