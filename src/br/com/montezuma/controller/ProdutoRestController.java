package br.com.montezuma.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.montezuma.dao.ProdutoDao;
import br.com.montezuma.modelo.Produto;

@RestController
public class ProdutoRestController {

    @Autowired
    ProdutoDao produtoService;

    @RequestMapping(value = "/produto/", method = RequestMethod.GET)
    public ResponseEntity<List<Produto>> listaTodosProdutos() {
	List<Produto> produtos = produtoService.buscaTodos();
	if (produtos.isEmpty()) {
	    return new ResponseEntity<List<Produto>>(HttpStatus.NO_CONTENT);
	}
	return new ResponseEntity<List<Produto>>(produtos, HttpStatus.OK);
    }

    @RequestMapping(value = "/produto/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Produto> getProduto(@PathVariable("id") long id) {
	System.out.println("Fetching Produto with id " + id);
	Produto produto = produtoService.findById(id);
	if (produto == null) {
	    System.out.println("Produto with id " + id + " not found");
	    return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
	}
	return new ResponseEntity<Produto>(produto, HttpStatus.OK);
    }

    @RequestMapping(value = "/produto/", method = RequestMethod.POST)
    public ResponseEntity<Void> criaProduto(@RequestBody Produto produto, UriComponentsBuilder ucBuilder) {
	System.out.println("Creating Produto " + produto.getNome());

	if (produtoService.buscaPeloNome(produto.getNome()) != null) {
	    System.out.println("A Produto with name " + produto.getNome() + " already exist");
	    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}

	produtoService.salva(produto);

	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(ucBuilder.path("/produto/{id}").buildAndExpand(produto.getId()).toUri());
	return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/produto/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Produto> atualizaProduto(@PathVariable("id") long id, @RequestBody Produto produto) {
	System.out.println("Updating Produto " + id);

	Produto currentProduto = produtoService.findById(id);

	if (currentProduto == null) {
	    System.out.println("Produto with id " + id + " not found");
	    return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
	}
	currentProduto.setNome(produto.getNome());
	currentProduto.setFabricante(produto.getFabricante());

	produtoService.atualiza(currentProduto);
	return new ResponseEntity<Produto>(currentProduto, HttpStatus.OK);
    }

    @RequestMapping(value = "/produto/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Produto> deletaProduto(@PathVariable("id") long id) {
	System.out.println("Fetching & Deleting Produto with id " + id);

	Produto produto = produtoService.findById(id);
	if (produto == null) {
	    System.out.println("Unable to delete. Produto with id " + id + " not found");
	    return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
	}

	produtoService.deletaPeloId(id);
	return new ResponseEntity<Produto>(HttpStatus.NO_CONTENT);
    }

}