package br.com.montezuma.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.montezuma.dao.ClienteDao;
import br.com.montezuma.dao.ProdutoDao;
import br.com.montezuma.dao.VendaDao;
import br.com.montezuma.modelo.Venda;

@RestController
public class VendaRestController {

    @Autowired
    VendaDao vendaDao;

    @Autowired
    ClienteDao clienteDao;

    @Autowired
    ProdutoDao produtoDao;

    @RequestMapping(value = "/venda/", method = RequestMethod.GET)
    public ResponseEntity<List<Venda>> listaTodasVendas() {
	List<Venda> vendas = vendaDao.buscaTodos();
	if (vendas.isEmpty()) {
	    return new ResponseEntity<List<Venda>>(HttpStatus.NO_CONTENT);
	}
	return new ResponseEntity<List<Venda>>(vendas, HttpStatus.OK);
    }

    @RequestMapping(value = "/venda/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Venda> getVenda(@PathVariable("id") long id) {
	System.out.println("Fetching Sale with id " + id);
	Venda venda = vendaDao.buscaPeloId(id);
	if (venda == null) {
	    System.out.println("Sale with id " + id + " not found");
	    return new ResponseEntity<Venda>(HttpStatus.NOT_FOUND);
	}
	return new ResponseEntity<Venda>(venda, HttpStatus.OK);
    }

    @RequestMapping(value = "/venda/", method = RequestMethod.POST)
    public ResponseEntity<Void> criaVenda(@RequestBody Venda venda, UriComponentsBuilder ucBuilder) {

	venda.setProduto(produtoDao.findById(venda.getProduto().getId()));
	venda.setCliente(clienteDao.buscaPeloId(venda.getCliente().getId()));

	vendaDao.salva(venda);

	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(ucBuilder.path("/venda/{id}").buildAndExpand(venda.getId()).toUri());
	return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/venda/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Venda> atualizaVenda(@PathVariable("id") long id, @RequestBody Venda venda) {
	System.out.println("Updating Sale " + id);

	Venda currentSale = vendaDao.buscaPeloId(id);

	if (currentSale == null) {
	    System.out.println("Sale with id " + id + " not found");
	    return new ResponseEntity<Venda>(HttpStatus.NOT_FOUND);
	}
	currentSale.setCliente(venda.getCliente());
	currentSale.setProduto(venda.getProduto());
	currentSale.setQuantitade(venda.getQuantidade());

	vendaDao.atualiza(currentSale);
	return new ResponseEntity<Venda>(currentSale, HttpStatus.OK);
    }

    @RequestMapping(value = "/venda/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Venda> deletaVenda(@PathVariable("id") long id) {
	System.out.println("Fetching & Deleting Sale with id " + id);

	Venda venda = vendaDao.buscaPeloId(id);
	if (venda == null) {
	    System.out.println("Unable to delete. Sale with id " + id + " not found");
	    return new ResponseEntity<Venda>(HttpStatus.NOT_FOUND);
	}

	vendaDao.delete(id);
	return new ResponseEntity<Venda>(HttpStatus.NO_CONTENT);
    }

}