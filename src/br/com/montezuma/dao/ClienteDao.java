package br.com.montezuma.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.montezuma.modelo.Cliente;

@Transactional
@Repository
public class ClienteDao {

    @PersistenceContext
    private EntityManager manager;

    public void salva(Cliente cliente) {
	manager.persist(cliente);
    }

    @SuppressWarnings("unchecked")
    public List<Cliente> buscaTodos() {
	return manager.createQuery("select t from Cliente t").getResultList();
    }

    public void delete(long id) {
	Cliente clientARemover = buscaPeloId(id);
	manager.remove(clientARemover);
    }

    public Cliente buscaPeloId(long id) {
	return manager.find(Cliente.class, id);
    }

    public void atualiza(Cliente cliente) {
	manager.merge(cliente);
    }

    @SuppressWarnings("unchecked")
    public Cliente buscaPeloNome(String nome) {
	List<Cliente> clientes = manager.createQuery(String.format("select t from Cliente t where t.nome='%s'", nome))
		.getResultList();
	if (clientes != null && clientes.size() > 0) {
	    return clientes.get(0);
	}
	return null;
    }

}
