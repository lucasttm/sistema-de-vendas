package br.com.montezuma.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.montezuma.modelo.Produto;

@Transactional
@Repository
public class ProdutoDao {

    @PersistenceContext
    private EntityManager manager;

    public void salva(Produto produto) {
	manager.persist(produto);
    }

    @SuppressWarnings("unchecked")
    public List<Produto> buscaTodos() {
	return manager.createQuery("select t from Produto t").getResultList();
    }

    public void deletaPeloId(long id) {
	Produto produtoARemover = findById(id);
	manager.remove(produtoARemover);
    }

    public Produto findById(long id) {
	return manager.find(Produto.class, id);
    }

    public void atualiza(Produto produto) {
	manager.merge(produto);
    }

    @SuppressWarnings("unchecked")
    public Produto buscaPeloNome(String nome) {
	List<Produto> produtos = manager.createQuery(String.format("select t from Produto t where t.nome='%s'", nome))
		.getResultList();
	if (produtos != null && produtos.size() > 0) {
	    return produtos.get(0);
	}
	return null;
    }

    public void deleteAllProdutos() {

    }

}
