package br.com.montezuma.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.montezuma.modelo.Venda;

@Transactional
@Repository
public class VendaDao {

    @PersistenceContext
    private EntityManager manager;

    public void salva(Venda venda) {
	manager.persist(venda);
    }

    @SuppressWarnings("unchecked")
    public List<Venda> buscaTodos() {
	return manager.createQuery("select t from Venda t").getResultList();
    }

    public void delete(long id) {
	Venda venda = buscaPeloId(id);
	manager.remove(venda);
    }

    public Venda buscaPeloId(long id) {
	return manager.find(Venda.class, id);
    }

    public void atualiza(Venda venda) {
	manager.merge(venda);
    }

}
