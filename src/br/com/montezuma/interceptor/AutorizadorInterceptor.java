package br.com.montezuma.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//� necess�rio registrar essa classe no XML
//Essa classe por ser um Filtro � chamada pelo Container em toda requisi��o
public class AutorizadorInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object controller) throws Exception {

		// Liberando acesso para p�gina de Login e resources(imagens, css, js)
		String uri = request.getRequestURI();
		if (uri.endsWith("loginForm") || uri.endsWith("efetuaLogin")
				|| uri.contains("resources")) {
			return true;
		}

		// Checando se tem usu�rio logado
		if (request.getSession().getAttribute("usuarioLogado") != null) {
			return true;
		}
		
		//Voltando para p�gina de login caso n�o tenha acesso
		response.sendRedirect("loginForm");
		return false;
	}
}
