package br.com.montezuma.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "produto")
public class Produto {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Column
    private String nome;

    @NotNull
    @Column
    private String fabricante;

    public Produto() {
	id = 0;
    }
    
    public Produto(String id){
    	this.id = Long.parseLong(id);
    }

    public Produto(long id, String nome, String fabricante) {
	super();
	this.id = id;
	this.nome = nome;
	this.fabricante = fabricante;
    }

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public String getNome() {
	return nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    public String getFabricante() {
	return fabricante;
    }

    public void setFabricante(String fabricante) {
	this.fabricante = fabricante;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (int) (id ^ (id >>> 32));
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Produto other = (Produto) obj;
	if (id != other.id) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Produto [id=" + id + ", nome=" + nome + ", fabricante=" + fabricante + "]";
    }

}
