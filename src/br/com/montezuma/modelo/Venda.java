package br.com.montezuma.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "venda")
public class Venda {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "cliente_id", referencedColumnName = "id")
    private Cliente cliente;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "produto_id", referencedColumnName = "id")
    private Produto produto;

    @NotNull
    @Column
    private int quantidade;

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    /**
     * @return the client
     */
    public Cliente getCliente() {
	return cliente;
    }

    /**
     * @param client
     *            the client to set
     */
    public void setCliente(Cliente cliente) {
	this.cliente = cliente;
    }

    public Produto getProduto() {
	return produto;
    }

    public void setProduto(Produto produto) {
	this.produto = produto;
    }

    public int getQuantidade() {
	return quantidade;
    }

    public void setQuantitade(int quantidade) {
	this.quantidade = quantidade;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + (int) (id ^ (id >>> 32));
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Venda other = (Venda) obj;
	if (id != other.id) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Venda [id=" + id + ", cliente=" + cliente + ", produto=" + produto + ", quantidade=" + quantidade + "]";
    }

}
